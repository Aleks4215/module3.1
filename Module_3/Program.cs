﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Module_3
{
    static class Program
    {
        static void Main(string[] args)
        {
            /// Use this method to implement tasks
        }

    }
    public class Task1
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// Throw ArgumentException if user input is invalid
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public int ParseAndValidateIntegerNumber(string source)
        {
            int result;
            bool isParsed = int.TryParse(source, out result);
            if (!isParsed)
            {
                throw new ArgumentException("Invalid number");
            }
            else
            {
                return result;
            }
        }

        public int Multiplication(int num1, int num2)
        {
            int result = 0;
            if (num1 != 0 && num2 != 0)
            {
                if (num1 > 0)
                {
                    for (int i = 0; i < num1; i++)
                    {
                        result = +num2;
                    }
                }
                else
                {
                    for (int i = 0; i > num1; i--)
                    {
                        result = -num2;
                    }
                }
            }
            return result;
        }
    }

    public class Task2
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            bool isParsed = int.TryParse(input, out result);
            if (result > 0 && isParsed)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<int> GetEvenNumbers(int naturalNumber)
        {
            List<int> evenNumbers = new List<int>();

            if (naturalNumber > 0)
            {
                for (int i = 1; i < naturalNumber; i++)
                {
                    if (i % 2 == 0)
                    {
                        evenNumbers.Add(i);
                    }
                }

            }
            else
            {
                Console.WriteLine("Enter value that more than 0");
                Console.ReadLine();
            }
            return evenNumbers;
        }
    }
    public class Task3
    {
        /// <summary>
        /// Use this method to parse and validate user input
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public bool TryParseNaturalNumber(string input, out int result)
        {
            bool isParsed = int.TryParse(input, out result);
            if (result > 0 && isParsed)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public string RemoveDigitFromNumber(int source, int digitToRemove)
        {
                string digit = source.ToString();
                char toRemove = (char)digitToRemove;
                foreach (var i in digit)
                {
                    int index = digit.IndexOf(toRemove);
                    if (index != -1)
                    {
                        int tempIndex = index;
                        char[] arr = digit.ToCharArray();
                        arr[tempIndex] = '\0';
                        digit = new string(arr);
                        return digit;
                    }
                    else
                    {
                        Console.WriteLine("Try one more time");
                        Console.ReadLine();
                        return "";
                    }
                }
            return digit;
        }
    }
}
